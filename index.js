exports.middleware = (store) => (next) => (action) => {
  if (action.type === "CONFIG_LOAD") {
    cfg = action.config;
    let fontSize = cfg.fontSize;
    action.config.fontSize = (cfg.hyperLineHeight.height * cfg.fontSize) + "px"; // change font-size
    action.config.termCSS = action.config.termCSS += `
      x-row {
        font-size: ${fontSize}px !important;
      }
    `
  }
  next(action);
}

exports.decorateTerm = (Term, { React, notify }) => {
  return class extends React.Component {
    constructor (props, context) {
      super(props, context);
      this._onTerminal = this._onTerminal.bind(this);
      this._onCursorChange = this._onCursorChange.bind(this);
      this._div = null;
    }

    _onTerminal (term) {
      if (this.props.onTerminal) this.props.onTerminal(term);
      this._div = term.div_;
      this._cursor = term.cursorNode_;
      this._observer = new MutationObserver(this._onCursorChange);
      this._observer.observe(this._cursor, {
        attributes: true,
        childList: false,
        characterData: false
      });

      self = this;
    }

    _onCursorChange () {
      let
        title = this._cursor.getAttribute("title"),
        curLeft = parseFloat(this._cursor.style.left);

      if (!title || curLeft === 0) return;

      let
        pos = title.match(/\((\d+), (\d+)\)/),
        left = parseInt(pos[2]),
        fontSize = parseFloat(cfg.fontSize) / cfg.hyperLineHeight.height,
        perChar = fontSize / cfg.hyperLineHeight.ratio;

      this._cursor.style.left = (left * perChar) + "px"
    }

    render () {
      return React.createElement(Term, Object.assign({}, this.props, {
        onTerminal: this._onTerminal
      }));
    }

    componentWillUnmount () {
      document.body.removeChild(this._canvas);
      if (this._observer) {
        this._observer.disconnect();
      }
    }
  }
};
