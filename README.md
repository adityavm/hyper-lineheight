# hyper-lineHeight

Allows a configurable line height for Hyper.

## Usage

Add `hyperLineHeight` to your config:

```javascript
module.exports = {
  config: {
    ...

    hyperLineHeight: {
      height: 1.5,  // the line height relative to font size
      ratio: 1.662  // ratio of font size to character width
    },

    ...
  }
}
```

## Known Issues

* Cursor tends to lead the text when entering long commands.
* Line breaks / text wrapping occurs much before the window edge.
